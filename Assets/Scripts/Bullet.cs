﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float destroyAfter = 5f;
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.transform);
        Destroy(gameObject);        
    }

    void Start()
    {
        StartCoroutine(DestroyAfter());
    }

    private IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(destroyAfter);
        Destroy(gameObject);
    }

}
